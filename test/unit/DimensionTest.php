<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest;

use ByteCube\ImageTools\Dimension;
use PHPUnit\Framework\TestCase;

class DimensionTest extends TestCase
{
    /**
     * @var Dimension
     */
    private $dimension;

    protected function setUp(): void
    {
        $this->dimension = new Dimension(0.2, 0.4, 0.6, 0.8);
    }

    public function testGetX()
    {
        $this->assertSame(0.2, $this->dimension->getX());
    }

    public function testGetY()
    {
        $this->assertSame(0.4, $this->dimension->getY());
    }

    public function testGetWidth()
    {
        $this->assertSame(0.6, $this->dimension->getWidth());
    }

    public function testGetHeight()
    {
        $this->assertSame(0.8, $this->dimension->getHeight());
    }

    public function testRotate()
    {
        $rotated = $this->dimension->rotate();

        $this->assertSame(0.4, $rotated->getX());
        $this->assertSame(0.2, $rotated->getY());
        $this->assertSame(0.8, $rotated->getWidth());
        $this->assertSame(0.6, $rotated->getHeight());
    }

    public function testGetAspectRatio()
    {
        $this->assertSame(0.6 / 0.8, $this->dimension->getAspectRatio()->get());
    }
}
