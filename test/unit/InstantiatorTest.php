<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest;

use ByteCube\ImageTools\Instantiator;
use PHPUnit\Framework\TestCase;

class InstantiatorTest extends TestCase
{
    public function testInstantiate()
    {
        $class = 'class InstantiatorTestClass {' .
            'public $value;' .
            'public function __construct($value) { $this->value = $value; }' .
            '}';

        eval($class);

        $instantiator = new Instantiator();

        $object = $instantiator->instantiate('InstantiatorTestClass', 'object-value');

        $this->assertSame('object-value', $object->value);
        $this->assertInstanceOf('InstantiatorTestClass', $object);
    }
}
