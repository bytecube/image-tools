<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\ElementBuilder;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\Element\SourceElement;
use ByteCube\ImageTools\ElementBuilder\SourceElementBuilder;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\Placeholder;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

class SourceElementBuilderTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testProcess()
    {
        $croppedImage = m::mock(ImageInterface::class);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->andReturn($croppedImage);

        $sourceElement = m::mock(SourceElement::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(SourceElement::class, [$croppedImage])->andReturn($sourceElement);

        $sourceElementBuilder = $this->createSourceElementBuilder($instantiator);
        $sourceElementBuilder->setImage($image);

        $this->assertSame($sourceElement, $sourceElementBuilder->build());
    }

    public function testProcessWithPlaceholder()
    {
        $croppedImage = m::mock(ImageInterface::class);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->andReturn($croppedImage);

        $placeholder = m::mock(Placeholder::class);

        $sourceElement = m::mock(SourceElement::class);
        $sourceElement
            ->shouldReceive('setPlaceholder')->with($placeholder);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(SourceElement::class, [$croppedImage])->andReturn($sourceElement);

        $sourceElementBuilder = $this->createSourceElementBuilder($instantiator);
        $sourceElementBuilder->setPlaceholder($placeholder);

        $this->assertSame($sourceElement, $sourceElementBuilder->build($image));
    }

    public function testProcessWithAddedTagBuilder()
    {
        $croppedImage = m::mock(ImageInterface::class);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->andReturn($croppedImage);

        $tag = m::mock(TagBuilder::class);

        $sourceElement = m::mock(SourceElement::class);
        $sourceElement
            ->shouldReceive('setTag')->with($tag);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(SourceElement::class, [$croppedImage])->andReturn($sourceElement);

        $sourceElementBuilder = $this->createSourceElementBuilder($instantiator);
        $sourceElementBuilder->setTag($tag);

        $this->assertSame($sourceElement, $sourceElementBuilder->build($image));
    }

    /**
     * @param Instantiator|m\Mock|null $instantiator
     *
     * @return SourceElementBuilder
     */
    protected function createSourceElementBuilder($instantiator = null)
    {
        if ($instantiator) {
            $instantiator
                ->shouldReceive('instantiate')->with(AspectRatio::class, 0.0, 0.0)->andReturn(new AspectRatio(0.0, 0.0))->getMock()
                ->shouldReceive('instantiate')->with(CropInstructions::class)->andReturn(new CropInstructions());
        }

        $image = m::mock(ImageInterface::class);

        $builder = new SourceElementBuilder($instantiator);
        $builder->setImage($image);

        return $builder;
    }
}
