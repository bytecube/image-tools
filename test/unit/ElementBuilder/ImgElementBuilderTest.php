<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\ElementBuilder;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\Element\ImgElement;
use ByteCube\ImageTools\ElementBuilder\ImgElementBuilder;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\Placeholder;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

class ImgElementBuilderTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testSetAFallbackWidthGreaterThanZero()
    {
        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(AspectRatio::class, 0.0, 0.0)->andReturn(new AspectRatio(0.0, 0.0));

        $imgElementBuilder = new ImgElementBuilder($instantiator);

        $this->assertSame(300, $imgElementBuilder->setFallbackWidth(300));
    }

    public function testSetAFallbackWidthLessThanZero()
    {
        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(AspectRatio::class, 0.0, 0.0)->andReturn(new AspectRatio(0.0, 0.0));

        $imgElementBuilder = new ImgElementBuilder($instantiator);

        $this->assertSame(0, $imgElementBuilder->setFallbackWidth(-100));
    }

    public function testProcess()
    {
        $croppedImage = m::mock(ImageInterface::class);

        $cropInstructions = new CropInstructions();

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->with($cropInstructions)->andReturn($croppedImage);

        $imgElement = m::mock(ImgElement::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(CropInstructions::class)->andReturn($cropInstructions)->getMock()
            ->shouldReceive('instantiate')->with(ImgElement::class, [$croppedImage], $croppedImage)->andReturn($imgElement);

        $imgElementBuilder = $this->createImgElementBuilder(300, $instantiator);
        $imgElementBuilder->setImage($image);

        $this->assertSame($imgElement, $imgElementBuilder->build());
    }

    public function testProcessWithPlaceholder()
    {
        $placeholder = m::mock(Placeholder::class);
        $croppedImage = m::mock(ImageInterface::class);

        $cropInstructions = new CropInstructions();

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->with($cropInstructions)->andReturn($croppedImage);

        $imgElement = m::mock(ImgElement::class);
        $imgElement
            ->shouldReceive('setPlaceholder')->with($placeholder);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(CropInstructions::class)->andReturn($cropInstructions)->getMock()
            ->shouldReceive('instantiate')->with(ImgElement::class, [$croppedImage], null)->andReturn($imgElement);

        $imgElementBuilder = $this->createImgElementBuilder(0, $instantiator);
        $imgElementBuilder->setPlaceholder($placeholder);

        $this->assertSame($imgElement, $imgElementBuilder->build($image));
    }

    public function testProcessWithAddedTagBuilder()
    {
        $tag = m::mock(TagBuilder::class);
        $croppedImage = m::mock(ImageInterface::class);

        $cropInstructions = new CropInstructions();

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->with($cropInstructions)->andReturn($croppedImage);

        $imgElement = m::mock(ImgElement::class);
        $imgElement
            ->shouldReceive('setTag')->with($tag);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(CropInstructions::class)->andReturn($cropInstructions)->getMock()
            ->shouldReceive('instantiate')->with(ImgElement::class, [$croppedImage], null)->andReturn($imgElement);

        $imgElementBuilder = $this->createImgElementBuilder(0, $instantiator);
        $imgElementBuilder->setTag($tag);

        $this->assertSame($imgElement, $imgElementBuilder->build($image));
    }

    /**
     * @param int                      $fallbackWidth
     * @param Instantiator|m\Mock|null $instantiator
     *
     * @return ImgElementBuilder
     */
    protected function createImgElementBuilder(int $fallbackWidth = 0, $instantiator = null)
    {
        $aspectRatio = new AspectRatio(3, 2);

        if ($instantiator) {
            $instantiator
                ->shouldReceive('instantiate')->with(AspectRatio::class, 0.0, 0.0)->andReturn($aspectRatio);
        }

        $builder = new ImgElementBuilder($instantiator);
        $builder->setFallbackWidth($fallbackWidth);
        $builder->setWidth(500);
        $builder->setCropName('-crop-name-');
        $builder->setAspectRatio($aspectRatio);
        $builder->setPixelDensities([1]);
        $builder->setRespectFocusArea(false);

        return $builder;
    }
}