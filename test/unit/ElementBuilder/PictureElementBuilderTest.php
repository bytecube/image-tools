<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\ElementBuilder;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Element\ImgElement;
use ByteCube\ImageTools\Element\PictureElement;
use ByteCube\ImageTools\Element\SourceElement;
use ByteCube\ImageTools\Element\SourceElementsGroup;
use ByteCube\ImageTools\ElementBuilder\ImgElementBuilder;
use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use ByteCube\ImageTools\ElementBuilder\SourceElementBuilder;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\Placeholder;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

class PictureElementBuilderTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testHasSourceElementBuilder()
    {
        $instantiator = m::mock(Instantiator::class);
        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);

        $this->assertFalse($pictureElementBuilder->hasSourceElementBuilder());
    }

    public function testHasImgElementBuilder()
    {
        $instantiator = m::mock(Instantiator::class);
        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);

        $this->assertFalse($pictureElementBuilder->hasImgElementBuilder());
    }

    public function testCreateSourceElementBuilder()
    {
        $sourceElementBuilder = m::mock(SourceElementBuilder::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(SourceElementBuilder::class)->andReturn($sourceElementBuilder);

        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);

        $this->assertSame($sourceElementBuilder, $pictureElementBuilder->createSourceElementBuilder());
        $this->assertTrue($pictureElementBuilder->hasSourceElementBuilder());
    }

    public function testCreateSourceElementBuilderPlaceholder()
    {
        $placeholder = m::mock(Placeholder::class);

        $sourceElementBuilder = m::mock(SourceElementBuilder::class);
        $sourceElementBuilder
            ->shouldReceive('setPlaceholder')->with($placeholder);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(SourceElementBuilder::class)->andReturn($sourceElementBuilder);

        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);
        $pictureElementBuilder->setPlaceholder($placeholder);
        $pictureElementBuilder->createSourceElementBuilder();
    }

    public function testCreateImgElementBuilder()
    {
        $imgElementBuilder = m::mock(ImgElementBuilder::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(ImgElementBuilder::class)->andReturn($imgElementBuilder);

        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);

        $this->assertSame($imgElementBuilder, $pictureElementBuilder->createImgElementBuilder());
        $this->assertTrue($pictureElementBuilder->hasImgElementBuilder());
    }

    public function testCreateImgElementBuilderWithPlaceholder()
    {
        $placeholder = m::mock(Placeholder::class);

        $imgElementBuilder = m::mock(ImgElementBuilder::class);
        $imgElementBuilder
            ->shouldReceive('setPlaceholder')->with($placeholder);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(ImgElementBuilder::class)->andReturn($imgElementBuilder);

        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);
        $pictureElementBuilder->setPlaceholder($placeholder);
        $pictureElementBuilder->createImgElementBuilder();
    }

    public function testBuild()
    {
        $croppedImage = m::mock(ImageInterface::class);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->andReturn($croppedImage);

        $imgElement = m::mock(ImgElement::class);

        $imgElementBuilder = m::mock(ImgElementBuilder::class);
        $imgElementBuilder
            ->shouldReceive('build')->with($image)->andReturn($imgElement);

        $sourceElement = m::mock(SourceElement::class);

        $sourceElementBuilder = m::mock(SourceElementBuilder::class);
        $sourceElementBuilder
            ->shouldReceive('build')->with($image)->andReturn($sourceElement);

        $sourceElementsGroup = m::mock(SourceElementsGroup::class);
        $sourceElementsGroup
            ->shouldReceive('add')->with($sourceElement);

        $pictureElement = m::mock(PictureElement::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(ImgElementBuilder::class)->andReturn($imgElementBuilder)->getMock()
            ->shouldReceive('instantiate')->with(SourceElementBuilder::class)->andReturn($sourceElementBuilder)->getMock()
            ->shouldReceive('instantiate')->with(SourceElementsGroup::class)->andReturn($sourceElementsGroup)->getMock()
            ->shouldReceive('instantiate')->with(PictureElement::class, $sourceElementsGroup, $imgElement)->andReturn($pictureElement);

        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);

        $pictureElementBuilder->createImgElementBuilder();
        $pictureElementBuilder->createSourceElementBuilder();

        $pictureElementBuilder->build($image);
    }

    public function testBuildWithAddedTagBuilder()
    {
        $croppedImage = m::mock(ImageInterface::class);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('crop')->andReturn($croppedImage);

        $aspectRatio = new AspectRatio(3, 2);

        $imgElement = m::mock(ImgElement::class);

        $imgElementBuilder = m::mock(ImgElementBuilder::class);
        $imgElementBuilder
            ->shouldReceive('build')->with($image)->andReturn($imgElement)->getMock()
            ->shouldReceive('hasCropName')->andReturnFalse()->getMock()
            ->shouldReceive('setCropName')->with('cropName')->getMock()
            ->shouldReceive('hasAspectRatio')->andReturnFalse()->getMock()
            ->shouldReceive('setAspectRatio')->with($aspectRatio)->andReturn($aspectRatio)->getMock()
            ->shouldReceive('hasPixelDensities')->andReturnFalse()->getMock()
            ->shouldReceive('setPixelDensities')->with([1, 2])->getMock()
            ->shouldReceive('hasRespectFocusArea')->andReturnFalse()->getMock()
            ->shouldReceive('setRespectFocusArea')->with(false)->getMock();

        $sourceElement = m::mock(SourceElement::class);

        $sourceElementBuilder = m::mock(SourceElementBuilder::class);
        $sourceElementBuilder
            ->shouldReceive('build')->with($image)->andReturn($sourceElement)->getMock()
            ->shouldReceive('hasCropName')->andReturnFalse()->getMock()
            ->shouldReceive('setCropName')->with('cropName')->getMock()
            ->shouldReceive('hasAspectRatio')->andReturnFalse()->getMock()
            ->shouldReceive('setAspectRatio')->with($aspectRatio)->andReturn($aspectRatio)->getMock()
            ->shouldReceive('hasPixelDensities')->andReturnFalse()->getMock()
            ->shouldReceive('setPixelDensities')->with([1, 2])->getMock()
            ->shouldReceive('hasRespectFocusArea')->andReturnFalse()->getMock()
            ->shouldReceive('setRespectFocusArea')->with(false)->getMock();

        $sourceElementsGroup = m::mock(SourceElementsGroup::class);
        $sourceElementsGroup
            ->shouldReceive('add')->with($sourceElement);

        $tag = m::mock(TagBuilder::class);

        $pictureElement = m::mock(PictureElement::class);
        $pictureElement
            ->shouldReceive('setTag')->with($tag);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(ImgElementBuilder::class)->andReturn($imgElementBuilder)->getMock()
            ->shouldReceive('instantiate')->with(SourceElementBuilder::class)->andReturn($sourceElementBuilder)->getMock()
            ->shouldReceive('instantiate')->with(SourceElementsGroup::class)->andReturn($sourceElementsGroup)->getMock()
            ->shouldReceive('instantiate')->with(PictureElement::class, $sourceElementsGroup, $imgElement)->andReturn($pictureElement);

        $pictureElementBuilder = $this->createPictureElementBuilder($instantiator);

        $pictureElementBuilder->setTag($tag);
        $pictureElementBuilder->setCropName('cropName');
        $pictureElementBuilder->setAspectRatio($aspectRatio);
        $pictureElementBuilder->setPixelDensities([1, 2]);
        $pictureElementBuilder->setRespectFocusArea(false);
        $pictureElementBuilder->createImgElementBuilder();
        $pictureElementBuilder->createSourceElementBuilder();

        $pictureElementBuilder->build($image);
    }

    /**
     * @param Instantiator|m\Mock|null $instantiator
     *
     * @return PictureElementBuilder
     */
    protected function createPictureElementBuilder($instantiator = null)
    {
        if ($instantiator) {
            $instantiator
                ->shouldReceive('instantiate')->with(AspectRatio::class, 0.0, 0.0)->andReturn(new AspectRatio(0.0, 0.0));
        }

        $builder = new PictureElementBuilder($instantiator);

        return $builder;
    }
}
