<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\ElementBuilder;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\ElementBuilder\AbstractElementBuilder;
use ByteCube\ImageTools\Placeholder;
use InvalidArgumentException;
use Mockery as m;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

class AbstractElementBuilderTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var AbstractElementBuilder|Mock
     */
    private $elementBuilder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->elementBuilder = m::mock(AbstractElementBuilder::class);
        $this->elementBuilder->makePartial();
    }

    public function testCheckEmptyPlaceholder()
    {
        $this->assertFalse($this->elementBuilder->hasPlaceholder());
    }

    public function testSetPlaceholderAndCheck()
    {
        $placeholder = m::mock(Placeholder::class);
        $this->elementBuilder->setPlaceholder($placeholder);

        $this->assertTrue($this->elementBuilder->hasPlaceholder());
        $this->assertSame($placeholder, $this->elementBuilder->getPlaceholder());
    }

    public function testCheckEmptyCropName()
    {
        $this->assertFalse($this->elementBuilder->hasCropName());
        $this->assertSame('', $this->elementBuilder->getCropName());
    }

    public function testSetCropNameAndCheck()
    {
        $this->elementBuilder->setCropName('crop-abc');
        $this->assertTrue($this->elementBuilder->hasCropName());
        $this->assertSame('crop-abc', $this->elementBuilder->getCropName());
    }

    public function testCheckEmptyAspectRatio()
    {
        $this->elementBuilder->__construct();

        $this->assertFalse($this->elementBuilder->hasAspectRatio());
    }

    public function testSetAspectRatioAndCheck()
    {
        $this->elementBuilder->setAspectRatio(new AspectRatio(1.0, 2.0));
        $this->assertTrue($this->elementBuilder->hasAspectRatio());
        $this->assertSame(1.0 / 2.0, $this->elementBuilder->getAspectRatio()->get());
    }

    public function testCheckEmptyPixelDensities()
    {
        $this->assertFalse($this->elementBuilder->hasPixelDensities());
        $this->assertSame([1], $this->elementBuilder->getPixelDensities());
    }

    public function testSetPixelDensitiesAndCheck()
    {
        $this->elementBuilder->setPixelDensities(['1', 2]);
        $this->assertTrue($this->elementBuilder->hasPixelDensities());
        $this->assertSame([1.0, 2.0], $this->elementBuilder->getPixelDensities());
    }

    public function testSetEmptyPixelDensitiesAndCheck()
    {
        $this->elementBuilder->setPixelDensities([]);
        $this->assertFalse($this->elementBuilder->hasPixelDensities());
        $this->assertSame([1], $this->elementBuilder->getPixelDensities());
    }

    public function testSetInvalidPixelDensities()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1536408342);
        $this->elementBuilder->setPixelDensities(['a', 'b']);
    }

    public function testCheckEmptyRespectFocusArea()
    {
        $this->elementBuilder->setRespectFocusArea(true);
        $this->assertTrue($this->elementBuilder->hasRespectFocusArea());
        $this->assertTrue($this->elementBuilder->getRespectFocusArea());
    }

    public function testSetTrueRespectFocusAreaAndCheck()
    {
        $this->elementBuilder->setRespectFocusArea(true);
        $this->assertTrue($this->elementBuilder->hasRespectFocusArea());
        $this->assertTrue($this->elementBuilder->getRespectFocusArea());
    }

    public function testSetFalseRespectFocusAreaAndCheck()
    {
        $this->elementBuilder->setRespectFocusArea(false);
        $this->assertTrue($this->elementBuilder->hasRespectFocusArea());
        $this->assertFalse($this->elementBuilder->getRespectFocusArea());
    }

    public function testCheckEmptyTag()
    {
        $this->assertFalse($this->elementBuilder->hasTag());
    }

    public function testSetTagAndCheck()
    {
        $tag = m::mock(TagBuilder::class);

        $this->elementBuilder->setTag($tag);
        $this->assertTrue($this->elementBuilder->hasTag());
        $this->assertSame($tag, $this->elementBuilder->getTag());
    }
}
