<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\ElementBuilder;

use ByteCube\ImageTools\ElementBuilder\AbstractImageElementBuilder;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class AbstractImageElementBuilderTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var AbstractImageElementBuilder
     */
    protected $imageElementBuilder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->imageElementBuilder = m::mock(AbstractImageElementBuilder::class)->makePartial();
    }

    public function testSetWidthGreaterThanZero()
    {
        $this->assertSame(300, $this->imageElementBuilder->setWidth(300));
    }

    public function testSetWidthLessThanZero()
    {
        $this->assertSame(0, $this->imageElementBuilder->setWidth(-300));
    }
}
