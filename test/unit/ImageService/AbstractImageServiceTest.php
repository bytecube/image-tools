<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\ImageService;

use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\ImageService\AbstractImageService;
use ByteCube\ImageTools\StorageInterface;
use InvalidArgumentException;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class AbstractImageServiceTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testCreateColorImage1()
    {
        $imageInfo = m::mock(ImageInfoInterface::class);
        $imageInfo
            ->shouldReceive('getWidth')->andReturn(400)->getMock()
            ->shouldReceive('getHeight')->andReturn(300)->getMock();

        $resultImage = m::mock(ImageInterface::class);

        $storage = m::mock(StorageInterface::class);
        $storage
            ->shouldReceive('hasImage')->with('image-tools/placeholder/color/123456_400x300.svg')->andReturnTrue()->getMock()
            ->shouldReceive('getImage')->with('image-tools/placeholder/color/123456_400x300.svg')->andReturn($resultImage)->getMock();

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getImageInfo')->withNoArgs()->andReturn($imageInfo)->getMock()
            ->shouldReceive('getStorage')->withNoArgs()->andReturn($storage)->getMock();

        $imageService = m::mock(AbstractImageService::class);
        $imageService->makePartial();

        $imageService->createColorImage($image, '123456');
    }

    public function testCreateColorImage2()
    {
        $imageInfo = m::mock(ImageInfoInterface::class);
        $imageInfo
            ->shouldReceive('getWidth')->andReturn(400)->getMock()
            ->shouldReceive('getHeight')->andReturn(300)->getMock();

        $resultImage = m::mock(ImageInterface::class);

        $saveContent = '<svg xmlns="http://www.w3.org/2000/svg" width="400" height="300" viewBox="0 0 400 300">' .
            '<rect width="400" height="300" fill="#112233"/>' .
            '</svg>';

        $storage = m::mock(StorageInterface::class);
        $storage
            ->shouldReceive('hasImage')->with('image-tools/placeholder/color/112233_400x300.svg')->andReturnFalse()->getMock()
            ->shouldReceive('saveImage')->with('image-tools/placeholder/color/112233_400x300.svg', $saveContent)->andReturn($resultImage)->getMock();

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getImageInfo')->withNoArgs()->andReturn($imageInfo)->getMock()
            ->shouldReceive('getStorage')->withNoArgs()->andReturn($storage)->getMock();

        $imageService = m::mock(AbstractImageService::class);
        $imageService->makePartial();

        $imageService->createColorImage($image, '123');
    }

    public function testCreateColorImage3()
    {
        $image = m::mock(ImageInterface::class);
        $imageService = m::mock(AbstractImageService::class);
        $imageService->makePartial();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1557942780);

        $imageService->createColorImage($image, '1234');
    }
}
