<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest;

use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\URLResolver;
use DateTime;
use InvalidArgumentException;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class URIResolverTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testResolveRelative()
    {
        $imageInfo1 = m::mock(ImageInfoInterface::class);
        $imageInfo1->shouldReceive('getRelativeURL')->andReturn('-image1-url-')->once();

        $image1 = m::mock(ImageInterface::class);
        $image1->shouldReceive('getImageInfo')->andReturn($imageInfo1);

        $imageInfo2 = m::mock(ImageInfoInterface::class);
        $imageInfo2->shouldReceive('getRelativeURL')->andReturn('-image2-url-')->once();

        $image2 = m::mock(ImageInterface::class);
        $image2->shouldReceive('getImageInfo')->andReturn($imageInfo2);

        $list = [$image1, [$image2]];

        $uriResolver = new URLResolver();

        $this->assertSame(
            [
                '-image1-url-',
                ['-image2-url-'],
            ],
            $uriResolver->resolveRelative($list)
        );
    }

    public function testResolveAbsolute()
    {
        $imageInfo1 = m::mock(ImageInfoInterface::class);
        $imageInfo1->shouldReceive('getAbsoluteURL')->andReturn('-image1-url-')->once();

        $image1 = m::mock(ImageInterface::class);
        $image1->shouldReceive('getImageInfo')->andReturn($imageInfo1);

        $imageInfo2 = m::mock(ImageInfoInterface::class);
        $imageInfo2->shouldReceive('getAbsoluteURL')->andReturn('-image2-url-')->once();

        $image2 = m::mock(ImageInterface::class);
        $image2->shouldReceive('getImageInfo')->andReturn($imageInfo2);

        $list = [$image1, [$image2]];

        $uriResolver = new URLResolver();

        $this->assertSame(
            [
                '-image1-url-',
                ['-image2-url-'],
            ],
            $uriResolver->resolveAbsolute($list)
        );
    }

    public function testWrongClassToResolve()
    {
        $list = [new DateTime()];
        $uriResolver = new URLResolver();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1558105700);

        $uriResolver->resolveAbsolute($list);
    }
}
