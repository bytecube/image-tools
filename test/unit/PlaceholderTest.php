<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest;

use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\Placeholder;
use InvalidArgumentException;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class PlaceholderTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testColor()
    {
        $placeholder = new Placeholder('#123456');
        $imageResult = m::mock(ImageInterface::class);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('createColorImage')->with('123456')->andReturn($imageResult);

        $placeholder->process($image);
    }

    public function testBlurred()
    {
        $placeholder = new Placeholder('blurred');
        $imageResult = m::mock(ImageInterface::class);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('blur')->withNoArgs()->andReturn($imageResult);

        $placeholder->process($image);
    }

    public function testInvalidPlaceholder()
    {
        $placeholder = new Placeholder('abc');
        $image = m::mock(ImageInterface::class);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1558022567);

        $placeholder->process($image);
    }
}
