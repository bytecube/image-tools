<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Crop;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Crop\Crop;
use ByteCube\ImageTools\Crop\CropDimensions;
use ByteCube\ImageTools\Dimension;
use ByteCube\ImageTools\Instantiator;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class CropTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testWithoutCropArea()
    {
        $dimension = new Dimension(0, 0, 1, 1);
        $cropDimensions = new CropDimensions(0, 0, 1000, 500);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(Dimension::class, 0, 0, 1, 1)->andReturn($dimension)->getMock()
            ->shouldReceive('instantiate')->with(CropDimensions::class, 0, 0, 1000, 500)->andReturn($cropDimensions);

        $crop = new Crop($instantiator);

        $actual = $crop->calculate(1000, 500);

        $this->assertEquals($cropDimensions, $actual);
    }

    public function testSimple()
    {
        $cropDimensions = new CropDimensions(200, 100, 600, 150);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(CropDimensions::class, 200, 100, 600, 150)->andReturn($cropDimensions);

        $cropArea = new Dimension(0.2, 0.2, 0.6, 0.3);

        $crop = (new Crop($instantiator))
            ->withCropArea($cropArea);

        $actual = $crop->calculate(1000, 500);

        $this->assertEquals($cropDimensions, $actual);
    }

    public function testAspectRatio()
    {
        $instantiator = m::mock(Instantiator::class);

        $this->addSourceAspectRatio($instantiator, 1000, 500);

        $dimension = new Dimension(0.15, 0.2, 0.4, 0.6);
        $cropDimensions = new CropDimensions(200, 75, 600, 200);

        $instantiator
            ->shouldReceive('instantiate')->with(CropDimensions::class, 200, 75, 600, 200)->andReturn($cropDimensions)->getMock()
            ->shouldReceive('instantiate')->with(Dimension::class, $this->floatMatcher(0.15), $this->floatMatcher(0.2), $this->floatMatcher(0.4), $this->floatMatcher(0.6))->andReturn($dimension);

        $cropArea = new Dimension(0.2, 0.2, 0.6, 0.3);
        $aspectRatio = new AspectRatio(3, 1);

        $crop = (new Crop($instantiator))
            ->withCropArea($cropArea)
            ->withAspectRatio($aspectRatio);

        $actual = $crop->calculate(1000, 500);

        $this->assertEquals($cropDimensions, $actual);
    }

    public function testAspectRatioAndFocusArea()
    {
        $instantiator = m::mock(Instantiator::class);

        $this->addSourceAspectRatio($instantiator, 1000, 500);

        $dimension = new Dimension(0.365, 0.2, 0.15, 0.3);
        $cropDimensions = new CropDimensions(365, 100, 150, 150);

        $instantiator
            ->shouldReceive('instantiate')->with(Dimension::class, $this->floatMatcher(0.365), $this->floatMatcher(0.2), $this->floatMatcher(0.15), $this->floatMatcher(0.3))->andReturn($dimension)->getMock()
            ->shouldReceive('instantiate')->with(CropDimensions::class, 365, 100, 150, 150)->andReturn($cropDimensions)->getMock();

        $cropArea = new Dimension(0.2, 0.2, 0.6, 0.3);
        $aspectRatio = new AspectRatio(1, 1);
        $focusArea = new Dimension(0.2, 0.4, 0.4, 0.3);

        $crop = (new Crop($instantiator))
            ->withCropArea($cropArea)
            ->withAspectRatio($aspectRatio)
            ->withFocusArea($focusArea);

        $actual = $crop->calculate(1000, 500);

        $this->assertEquals($cropDimensions, $actual);
    }

    public function testWithoutFocusArea()
    {
        $instantiator = m::mock(Instantiator::class);

        $this->addSourceAspectRatio($instantiator, 1000, 500);

        $dimension = new Dimension(0.425, 0.2, 0.15, 0.3);
        $cropDimensions = new CropDimensions(425, 100, 150, 150);

        $instantiator
            ->shouldReceive('instantiate')->with(Dimension::class, $this->floatMatcher(0.425), $this->floatMatcher(0.2), $this->floatMatcher(0.15), $this->floatMatcher(0.3))->andReturn($dimension)->getMock()
            ->shouldReceive('instantiate')->with(CropDimensions::class, 425, 100, 150, 150)->andReturn($cropDimensions)->getMock();

        $cropArea = new Dimension(0.2, 0.2, 0.6, 0.3);
        $aspectRatio = new AspectRatio(1, 1);
        $focusArea = new Dimension(0.2, 0.4, 0.4, 0.3);

        $crop = (new Crop($instantiator))
            ->withCropArea($cropArea)
            ->withAspectRatio($aspectRatio)
            ->withFocusArea($focusArea)
            ->withoutFocusArea();

        $actual = $crop->calculate(1000, 500);

        $this->assertEquals($cropDimensions, $actual);
    }

    public function testBigFocus()
    {
        $instantiator = m::mock(Instantiator::class);

        $this->addSourceAspectRatio($instantiator, 500, 1000);

        $dimension = new Dimension(0.2888, 0.2, 0.2, 0.3);
        $cropDimensions = new CropDimensions(144, 200, 100, 300);

        $instantiator
            ->shouldReceive('instantiate')->with(Dimension::class, $this->floatMatcher(0.2888), $this->floatMatcher(0.2), $this->floatMatcher(0.2), $this->floatMatcher(0.3))->andReturn($dimension)->getMock()
            ->shouldReceive('instantiate')->with(CropDimensions::class, 144, 200, 100, 300)->andReturn($cropDimensions)->getMock();

        $cropArea = new Dimension(0.2, 0.2, 0.6, 0.3);
        $aspectRatio = new AspectRatio(1, 3);
        $focusArea = new Dimension(0.2, 0.3, 0.1, 0.4);

        $crop = (new Crop($instantiator))
            ->withCropArea($cropArea)
            ->withAspectRatio($aspectRatio)
            ->withFocusArea($focusArea);

        $actual = $crop->calculate(500, 1000);

        $this->assertEquals($cropDimensions, $actual);
    }

    protected function addSourceAspectRatio(m\MockInterface $instantiator, int $width, int $height)
    {
        $sourceAspectRatio = new AspectRatio($width, $height);

        $instantiator->shouldReceive('instantiate')->with(AspectRatio::class, $width, $height)->andReturn($sourceAspectRatio);
    }

    protected function floatMatcher(float $expected): m\Matcher\Closure
    {
        return m::on(function ($argument) use ($expected) {
            return abs($expected - $argument) < 0.001;
        });
    }
}
