<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Crop;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Crop\CropInstructions;
use PHPUnit\Framework\TestCase;

class CropInstructionsTest extends TestCase
{
    /**
     * @var CropInstructions
     */
    protected $cropInstructions;

    protected function setUp(): void
    {
        $this->cropInstructions = new CropInstructions();
    }

    /**
     * @test
     */
    public function cropName()
    {
        $this->cropInstructions->setCropName('abc');
        $this->assertSame('abc', $this->cropInstructions->getCropName());
    }

    /**
     * @test
     */
    public function width()
    {
        $this->cropInstructions->setWidth(123.45);
        $this->assertSame(123.45, $this->cropInstructions->getWidth());
    }

    /**
     * @test
     */
    public function pixelDensity()
    {
        $this->cropInstructions->setPixelDensity(4.5);
        $this->assertSame(4.5, $this->cropInstructions->getPixelDensity());
    }

    /**
     * @test
     */
    public function aspectRatio()
    {
        $this->cropInstructions->setAspectRatio(new AspectRatio(5.0, 3.0));
        $this->assertSame(5.0 / 3.0, $this->cropInstructions->getAspectRatio()->get());
    }

    /**
     * @test
     */
    public function respectFocusArea()
    {
        $this->cropInstructions->setRespectFocusArea(false);
        $this->assertFalse($this->cropInstructions->isRespectFocusArea());
    }
}
