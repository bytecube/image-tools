<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Crop;

use ByteCube\ImageTools\Crop\Crop;
use ByteCube\ImageTools\Crop\CropCollection;
use ByteCube\ImageTools\Instantiator;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class CropCollectionTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testGet()
    {
        $crop = m::mock(Crop::class);

        $cropCollection = new CropCollection();
        $cropCollection->add('crop', $crop);

        $this->assertSame($crop, $cropCollection->get('crop'));
    }

    public function testGetEmpty()
    {
        $crop = m::mock(Crop::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(Crop::class)->andReturn($crop);

        $cropCollection = new CropCollection($instantiator);

        $this->assertSame($cropCollection->get('crop'), $cropCollection->get('crop'));
    }
}
