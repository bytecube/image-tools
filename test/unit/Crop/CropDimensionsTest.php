<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Crop;

use ByteCube\ImageTools\Crop\CropDimensions;
use PHPUnit\Framework\TestCase;

class CropDimensionsTest extends TestCase
{
    /**
     * @var CropDimensions
     */
    private $cropDimensions;

    protected function setUp(): void
    {
        $this->cropDimensions = new CropDimensions(2, 4, 6, 8);
    }

    public function testGetX()
    {
        $this->assertSame(2, $this->cropDimensions->getX());
    }

    public function testGetY()
    {
        $this->assertSame(4, $this->cropDimensions->getY());
    }

    public function testGetWidth()
    {
        $this->assertSame(6, $this->cropDimensions->getWidth());
    }

    public function testGetHeight()
    {
        $this->assertSame(8, $this->cropDimensions->getHeight());
    }
}
