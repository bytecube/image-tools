<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Crop\Crop;
use ByteCube\ImageTools\Crop\CropCollection;
use ByteCube\ImageTools\Crop\CropDimensions;
use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\ImageService\ImageServiceInterface;
use ByteCube\ImageTools\Processor;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class ProcessorTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testCrop()
    {
        $aspectRatio = new AspectRatio(1, 1);
        $cropDimensions = new CropDimensions(1, 2, 3, 4);

        $crop = m::mock(Crop::class);
        $crop
            ->shouldReceive('withAspectRatio')->with($aspectRatio)->andReturnSelf()->once()->getMock()
            ->shouldReceive('withoutFocusArea')->withNoArgs()->andReturnSelf()->getMock()
            ->shouldReceive('calculate')->with(3500, 2000)->andReturn($cropDimensions)->getMock();

        $cropCollection = m::mock(CropCollection::class);
        $cropCollection
            ->shouldReceive('get')->with('cropName')->andReturn($crop)->once()->getMock();

        $imageInfo = m::mock(ImageInfoInterface::class);
        $imageInfo
            ->shouldReceive('getWidth')->andReturn(3500)->once()->getMock()
            ->shouldReceive('getHeight')->andReturn(2000)->once()->getMock();

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getCrop')->andReturn($cropCollection)->once()->getMock()
            ->shouldReceive('getImageInfo')->andReturn($imageInfo)->once()->getMock();

        $imageResult = m::mock(ImageInterface::class);

        $imageService = m::mock(ImageServiceInterface::class);
        $imageService
            ->shouldReceive('crop')->with($image, 3500.0, 11.5, $cropDimensions)->andReturn($imageResult)->getMock();

        $processor = new Processor($image, $imageService);

        $cropInstructions = new CropInstructions();
        $cropInstructions->setCropName('cropName');
        $cropInstructions->setAspectRatio($aspectRatio);
        $cropInstructions->setRespectFocusArea(false);
        $cropInstructions->setWidth(350.0);
        $cropInstructions->setPixelDensity(11.5);

        $processor->crop($cropInstructions);
    }

    public function testBlur()
    {
        $image = m::mock(ImageInterface::class);
        $imageResult = m::mock(ImageInterface::class);

        $imageService = m::mock(ImageServiceInterface::class);
        $imageService
            ->shouldReceive('blur')->with($image)->andReturn($imageResult)->getMock();

        $processor = new Processor($image, $imageService);

        $processor->blur();
    }

    public function testCreateColorImage()
    {
        $image = m::mock(ImageInterface::class);
        $imageResult = m::mock(ImageInterface::class);

        $imageService = m::mock(ImageServiceInterface::class);
        $imageService
            ->shouldReceive('createColorImage')->with($image, '123456')->andReturn($imageResult)->getMock();

        $processor = new Processor($image, $imageService);

        $processor->createColorImage('123456');
    }
}
