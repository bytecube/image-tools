<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Element;

use ByteCube\ImageTools\Element\ImgElement;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\Placeholder;
use ByteCube\ImageTools\URLResolver;
use InvalidArgumentException;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

class ImgElementTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testGetFallback()
    {
        $processedImages = [m::mock(ImageInterface::class), m::mock(ImageInterface::class)];
        $fallbackImage = m::mock(ImageInterface::class);

        $urlResolver = m::mock(URLResolver::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $imgElement = new ImgElement($processedImages, $fallbackImage, $instantiator);

        $this->assertSame($fallbackImage, $imgElement->getFallback());
    }

    public function testTwoProcessedImagesAndNoFallback()
    {
        $processedImages = [m::mock(ImageInterface::class), m::mock(ImageInterface::class)];

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1536408374);

        new ImgElement($processedImages);
    }

    public function testRenderRelative()
    {
        $image = m::mock(ImageInterface::class);

        $urlResolver = m::mock(URLResolver::class);
        $urlResolver
            ->shouldReceive('resolveRelative')->with(['src' => $image])->andReturn(['src' => '-relative-url-']);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $imgElement = new ImgElement([$image], null, $instantiator);

        $this->assertSame('<img src="-relative-url-" />', $imgElement->renderRelative());
    }

    public function testRenderAbsolute()
    {
        $image1 = m::mock(ImageInterface::class);
        $image1
            ->shouldReceive('getPixelDensity')->withNoArgs()->andReturn(1);

        $image2 = m::mock(ImageInterface::class);
        $image2
            ->shouldReceive('getPixelDensity')->withNoArgs()->andReturn(2);

        $fallbackImage = m::mock(ImageInterface::class);

        $urlResolver = m::mock(URLResolver::class);
        $urlResolver
            ->shouldReceive('resolveAbsolute')->with(['src' => $fallbackImage, 'srcset' => [1 => $image1, 2 => $image2]])->andReturn(['src' => '-fallback-url-', 'srcset' => [1 => '-url1-', 2 => '-url2-']]);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $imgElement = new ImgElement([$image1, $image2], $fallbackImage, $instantiator);
        $imgElement->setTag(new TagBuilder());

        $this->assertSame('<img src="-fallback-url-" srcset="-url1-, -url2- 2x" />', $imgElement->renderAbsolute());
    }

    public function testRenderWithPlaceholder()
    {
        $placeholderImageInfo = m::mock(ImageInfoInterface::class);
        $placeholderImageInfo
            ->shouldReceive('getAbsoluteURL')->withNoArgs()->andReturn('-placeholder-url-')->getMock();

        $placeholderImage = m::mock(ImageInterface::class);
        $placeholderImage
            ->shouldReceive('getImageInfo')->withNoArgs()->andReturn($placeholderImageInfo)->getMock();

        $placeholder = m::mock(Placeholder::class);
        $placeholder
            ->shouldReceive('process')->andReturn($placeholderImage);

        $image = m::mock(ImageInterface::class);

        $urlResolver = m::mock(URLResolver::class);
        $urlResolver
            ->shouldReceive('resolveAbsolute')->with(['src' => $image])->andReturn(['src' => '-url-']);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $imgElement = new ImgElement([$image], null, $instantiator);
        $imgElement->setPlaceholder($placeholder);

        $this->assertSame('<img src="-placeholder-url-" data-src="-url-" />', $imgElement->renderAbsolute());
    }
}
