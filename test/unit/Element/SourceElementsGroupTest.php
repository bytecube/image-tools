<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Element;

use ByteCube\ImageTools\Element\SourceElement;
use ByteCube\ImageTools\Element\SourceElementsGroup;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class SourceElementsGroupTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testAdd()
    {
        $sourceElementsGroup = new SourceElementsGroup();

        $sourceElement = m::mock(SourceElement::class);

        $sourceElementsGroup->add($sourceElement);

        $this->assertSame([$sourceElement], $sourceElementsGroup->getElements());
        $this->assertFalse($sourceElementsGroup->isEmpty());
    }

    public function testRenderRelative()
    {
        $sourceElementsGroup = $this->setUpRender('renderRelative');

        $this->assertSame('-source-tag1--source-tag2-', $sourceElementsGroup->renderRelative());
    }

    public function testRenderAbsolute()
    {
        $sourceElementsGroup = $this->setUpRender('renderAbsolute');

        $this->assertSame('-source-tag1--source-tag2-', $sourceElementsGroup->renderAbsolute());
    }

    public function testGetImageUris()
    {
        $sourceElement1 = m::mock(SourceElement::class);
        $sourceElement1
            ->shouldReceive('getImageList')->withNoArgs()->andReturn(['img-list-1']);
        $sourceElement2 = m::mock(SourceElement::class);
        $sourceElement2
            ->shouldReceive('getImageList')->withNoArgs()->andReturn(['img-list-2']);

        $sourceElementsGroup = new SourceElementsGroup();
        $sourceElementsGroup->add($sourceElement1);
        $sourceElementsGroup->add($sourceElement2);

        $expected = [['img-list-1'], ['img-list-2']];
        $this->assertSame($expected, $sourceElementsGroup->getImageList());
    }

    protected function setUpRender(string $renderMethod)
    {
        $sourceElementsGroup = new SourceElementsGroup();

        [$sourceElement1, $sourceElement2] = $this->createSourceElements($renderMethod);

        $sourceElementsGroup->add($sourceElement1);
        $sourceElementsGroup->add($sourceElement2);

        return $sourceElementsGroup;
    }

    protected function createSourceElements(string $renderMethod)
    {
        return [
            m::mock(SourceElement::class)->shouldReceive($renderMethod)->andReturn('-source-tag1-')->getMock(),
            m::mock(SourceElement::class)->shouldReceive($renderMethod)->andReturn('-source-tag2-')->getMock(),
        ];
    }

    protected function setUpImageUris()
    {
        $sourceElement = m::mock(SourceElement::class);

        $sourceElementsGroup = new SourceElementsGroup();
        $sourceElementsGroup->add($sourceElement);

        return $sourceElementsGroup;
    }
}
