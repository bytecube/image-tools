<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Element;

use ByteCube\ImageTools\Element\SourceElement;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\Placeholder;
use ByteCube\ImageTools\URLResolver;
use InvalidArgumentException;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class SourceElementTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testGetProcessedImages()
    {
        $urlResolver = m::mock(URLResolver::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $image = m::mock(ImageInterface::class);
        $sourceElement = new SourceElement([$image], $instantiator);

        $this->assertSame([$image], $sourceElement->getImages());
    }

    public function testRenderRelative()
    {
        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getPixelDensity')->withNoArgs()->andReturn(1)->getMock();

        $urlResolver = m::mock(URLResolver::class);
        $urlResolver
            ->shouldReceive('resolveRelative')->with([1 => $image])->andReturn([1 => '-image-url-']);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $sourceElement = new SourceElement([$image], $instantiator);

        $this->assertSame('<source srcset="-image-url-" />', $sourceElement->renderRelative());
    }

    public function testRenderAbsolute()
    {
        $image1 = m::mock(ImageInterface::class);
        $image1
            ->shouldReceive('getPixelDensity')->withNoArgs()->andReturn(1)->getMock();

        $image2 = m::mock(ImageInterface::class);
        $image2
            ->shouldReceive('getPixelDensity')->withNoArgs()->andReturn(2)->getMock();

        $urlResolver = m::mock(URLResolver::class);
        $urlResolver
            ->shouldReceive('resolveAbsolute')->with([1 => $image1, 2 => $image2])->andReturn([1 => '-image-url-1-', 2 => '-image-url-2-']);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $sourceElement = new SourceElement([$image1, $image2], $instantiator);

        $this->assertSame('<source srcset="-image-url-1-, -image-url-2- 2x" />', $sourceElement->renderAbsolute());
    }

    public function testRenderWithPlaceholder()
    {
        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getPixelDensity')->withNoArgs()->andReturn(1)->getMock();

        $placeholderImageInfo = m::mock(ImageInfoInterface::class);
        $placeholderImageInfo
            ->shouldReceive('getRelativeURL')->withNoArgs()->andReturn('-placeholder-url-')->getMock();

        $placeholderImage = m::mock(ImageInterface::class);
        $placeholderImage
            ->shouldReceive('getImageInfo')->withNoArgs()->andReturn($placeholderImageInfo)->getMock();

        $placeholder = m::mock(Placeholder::class);
        $placeholder
            ->shouldReceive('process')->with($image)->andReturn($placeholderImage);

        $urlResolver = m::mock(URLResolver::class);
        $urlResolver
            ->shouldReceive('resolveRelative')->with([1 => $image])->andReturn([1 => '-image-url-']);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $sourceElement = new SourceElement([$image], $instantiator);
        $sourceElement->setPlaceholder($placeholder);

        $this->assertSame('<source srcset="-placeholder-url-" data-srcset="-image-url-" />', $sourceElement->renderRelative());
    }

    public function testWithoutProcessedImages()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1536408398);
        new SourceElement([]);
    }
}
