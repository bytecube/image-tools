<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Element;

use ByteCube\ImageTools\Element\ImgElement;
use ByteCube\ImageTools\Element\PictureElement;
use ByteCube\ImageTools\Element\SourceElementsGroup;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\URLResolver;
use InvalidArgumentException;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

class PictureElementTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testGetTag()
    {
        $sourceElementsGroup = m::mock(SourceElementsGroup::class);
        $sourceElementsGroup
            ->shouldReceive('isEmpty')->withNoArgs()->andReturnFalse()->getMock();

        $imgElement = m::mock(ImgElement::class);

        $tag = m::mock(TagBuilder::class);

        $urlResolver = m::mock(URLResolver::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $pictureElement = new PictureElement($sourceElementsGroup, $imgElement, $instantiator);
        $pictureElement->setTag($tag);
        $this->assertSame($tag, $pictureElement->getTag());
    }

    public function testGetSourceElementsAndGetImgElement()
    {
        $sourceElementsGroup = m::mock(SourceElementsGroup::class);
        $sourceElementsGroup
            ->shouldReceive('isEmpty')->withNoArgs()->andReturnFalse()->getMock();

        $imgElement = m::mock(ImgElement::class);

        $urlResolver = m::mock(URLResolver::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $pictureElement = new PictureElement($sourceElementsGroup, $imgElement, $instantiator);

        $this->assertSame($sourceElementsGroup, $pictureElement->getSourceElements());
        $this->assertSame($imgElement, $pictureElement->getImgElement());
    }

    public function testRender()
    {
        $tag = m::mock(TagBuilder::class);
        $tag
            ->shouldReceive('setTagName')->with('picture')->getMock()
            ->shouldReceive('setContent')->with('<source><source><img>')->getMock()
            ->shouldReceive('render')->withNoArgs()->andReturn('<picture>');

        $sourceElementsGroup = m::mock(SourceElementsGroup::class);
        $sourceElementsGroup
            ->shouldReceive('isEmpty')->withNoArgs()->andReturnFalse()->getMock()
            ->shouldReceive('renderRelative')->andReturn('<source><source>')->getMock()
            ->shouldReceive('renderAbsolute')->andReturn('<source><source>')->getMock();

        $imgElement = m::mock(ImgElement::class);
        $imgElement
            ->shouldReceive('renderRelative')->withNoArgs()->andReturn('<img>')->getMock()
            ->shouldReceive('renderAbsolute')->withNoArgs()->andReturn('<img>')->getMock();

        $urlResolver = m::mock(URLResolver::class);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $pictureElement = new PictureElement($sourceElementsGroup, $imgElement, $instantiator);
        $pictureElement->setTag($tag);

        $this->assertSame('<picture>', $pictureElement->renderRelative());
        $this->assertSame('<picture>', $pictureElement->renderAbsolute());
    }

    public function testGetRelativeImageURLs()
    {
        $sourceImage1 = m::mock(ImageInterface::class);
        $sourceImage2 = m::mock(ImageInterface::class);

        $sourceElementsGroup = m::mock(SourceElementsGroup::class);
        $sourceElementsGroup
            ->shouldReceive('isEmpty')->withNoArgs()->andReturnFalse()->getMock()
            ->shouldReceive('getImageList')->withNoArgs()->andReturn([[1 => $sourceImage1, 2 => $sourceImage2]])->getMock();

        $imgImage = m::mock(ImageInterface::class);

        $imgElement = m::mock(ImgElement::class);
        $imgElement
            ->shouldReceive('getImageList')->withNoArgs()->andReturn(['src' => $imgImage]);

        $urlResolver = m::mock(URLResolver::class);
        $urlResolver
            ->shouldReceive('resolveRelative')->with(['sources' => [[1 => $sourceImage1, 2 => $sourceImage2]], 'img' => ['src' => $imgImage]])->andReturn(['-image-urls-']);

        $instantiator = m::mock(Instantiator::class);
        $instantiator
            ->shouldReceive('instantiate')->with(URLResolver::class)->andReturn($urlResolver);

        $pictureElement = new PictureElement($sourceElementsGroup, $imgElement, $instantiator);

        $this->assertSame(['-image-urls-'], $pictureElement->getRelativeImageURLs());
    }

    public function testWithoutSourceElements()
    {
        $imgElement = m::mock(ImgElement::class);
        $sourceElementsGroup = m::mock(SourceElementsGroup::class);
        $sourceElementsGroup
            ->shouldReceive('isEmpty')->andReturnTrue();

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1536408386);

        new PictureElement($sourceElementsGroup, $imgElement);
    }
}
