<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest\Image;

use ByteCube\ImageTools\Crop\CropCollection;
use ByteCube\ImageTools\Image\Image;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\ImageService\ImageServiceInterface;
use ByteCube\ImageTools\StorageInterface;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use stdClass;

class ImageTest extends TestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function test()
    {
        $imageInfo = m::mock(ImageInfoInterface::class);
        $imageInfo
            ->shouldReceive('getWidth')->andReturn(400)->once()->getMock()
            ->shouldReceive('getHeight')->andReturn(300)->once()->getMock();

        $original = new stdClass();
        $storage = m::mock(StorageInterface::class);
        $imageService = m::mock(ImageServiceInterface::class);

        $image = new Image($imageInfo, $original, $storage, $imageService);

        $this->assertSame($imageInfo, $image->getImageInfo());
        $this->assertSame(400 / 300, $image->getAspectRatio()->get());
        $this->assertSame(1.0, $image->getPixelDensity());
        $this->assertInstanceOf(CropCollection::class, $image->getCrop());
        $this->assertSame($original, $image->getOriginal());
        $this->assertSame($storage, $image->getStorage());
    }
}
