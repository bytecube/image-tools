<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\UnitTest;

use ByteCube\ImageTools\AspectRatio;
use PHPUnit\Framework\TestCase;

class AspectRatioTest extends TestCase
{
    /**
     * @var AspectRatio
     */
    private $aspectRatio;

    protected function setUp(): void
    {
        $this->aspectRatio = new AspectRatio(16, 9);
    }

    public function testGet()
    {
        $this->assertSame(16 / 9, $this->aspectRatio->get());
    }

    public function testRotate()
    {
        $this->assertSame(9 / 16, $this->aspectRatio->rotate()->get());
    }

    public function testIsInvalid()
    {
        $this->assertTrue($this->aspectRatio->isValid());

        $this->assertFalse((new AspectRatio(-1, 2))->isValid());
        $this->assertFalse((new AspectRatio(0, 2))->isValid());
        $this->assertFalse((new AspectRatio(2, -1))->isValid());
        $this->assertFalse((new AspectRatio(2, 0))->isValid());
    }
}
