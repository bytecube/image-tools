<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools;

final class AspectRatio
{
    /**
     * @var Instantiator
     */
    private $instantiator;

    /**
     * @var float
     */
    private $width;

    /**
     * @var float
     */
    private $height;

    public function __construct(float $width, float $height, Instantiator $instantiator = null)
    {
        $this->instantiator = $instantiator ?: new Instantiator();
        $this->width = $width;
        $this->height = $height;
    }

    public function get(): float
    {
        return $this->width / $this->height;
    }

    public function rotate(): AspectRatio
    {
        return $this->instantiator->instantiate(AspectRatio::class, $this->height, $this->width);
    }

    public function isValid(): bool
    {
        return $this->width > 0 && $this->height > 0;
    }
}
