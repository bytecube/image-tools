<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\ImageService;

use ByteCube\ImageTools\Image\ImageInterface;
use InvalidArgumentException;

abstract class AbstractImageService implements ImageServiceInterface
{
    public function createColorImage(ImageInterface $image, string $color): ImageInterface
    {
        if (!preg_match('/^([a-f0-9]){3}(([a-f0-9]){3})?$/i', $color)) {
            throw new InvalidArgumentException('Invalid color pattern "' . $color . '".', 1557942780);
        }

        $color = $this->reformatColor($color);

        $width = $image->getImageInfo()->getWidth();
        $height = $image->getImageInfo()->getHeight();
        $storage = $image->getStorage();

        $fileName = $this->generateColorFileName($color, $width, $height);

        $fileIdentifier = 'image-tools/placeholder/color/' . $fileName;

        if ($storage->hasImage($fileIdentifier)) {
            return $storage->getImage($fileIdentifier);
        }

        $fileContent = $this->generateSvgContent($width, $height, $color);

        return $storage->saveImage($fileIdentifier, $fileContent);
    }

    protected function reformatColor(string $color): string
    {
        if (strlen($color) === 6) {
            $red = hexdec(substr($color, 0, 2));
            $green = hexdec(substr($color, 2, 2));
            $blue = hexdec(substr($color, 4, 2));
        } else {
            $red = hexdec($color[0] . $color[0]);
            $green = hexdec($color[1] . $color[1]);
            $blue = hexdec($color[2] . $color[2]);
        }

        return str_pad(dechex($red), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex($green), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex($blue), 2, '0', STR_PAD_LEFT);
    }

    protected function generateColorFileName(string $color, int $width, int $height)
    {
        return $color . '_' . $width . 'x' . $height . '.svg';
    }

    protected function generateSvgContent(int $width, int $height, string $color)
    {
        return '<svg xmlns="http://www.w3.org/2000/svg" width="' . $width . '" height="' . $height . '" viewBox="0 0 ' . $width . ' ' . $height . '">' .
            '<rect width="' . $width . '" height="' . $height . '" fill="#' . $color . '"/>' .
            '</svg>';
    }
}
