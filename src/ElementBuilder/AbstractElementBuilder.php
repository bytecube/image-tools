<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\ElementBuilder;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\Placeholder;
use InvalidArgumentException;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

abstract class AbstractElementBuilder
{
    /**
     * @var Instantiator
     */
    protected $instantiator;

    /**
     * @var Placeholder
     */
    protected $placeholder;

    /**
     * @var string
     */
    protected $cropName = '';

    /**
     * @var AspectRatio
     */
    protected $aspectRatio;

    /**
     * @var float[]
     */
    protected $pixelDensities = [];

    /**
     * @var bool
     */
    protected $respectFocusArea;

    /**
     * @var TagBuilder
     */
    protected $tag;

    public function __construct(Instantiator $instantiator = null)
    {
        $this->instantiator = $instantiator ?: new Instantiator();
        $this->aspectRatio = $this->instantiator->instantiate(AspectRatio::class, 0.0, 0.0);
    }

    public function hasPlaceholder(): bool
    {
        return !!$this->placeholder;
    }

    public function setPlaceholder(Placeholder $placeholder): Placeholder
    {
        $this->placeholder = $placeholder;

        return $this->placeholder;
    }

    public function getPlaceholder(): Placeholder
    {
        return $this->placeholder;
    }

    public function hasCropName(): bool
    {
        return $this->cropName !== '';
    }

    public function getCropName(): string
    {
        return $this->cropName;
    }

    public function setCropName(string $cropName): string
    {
        $this->cropName = $cropName;

        return $this->cropName;
    }

    public function hasAspectRatio(): bool
    {
        return $this->aspectRatio->isValid();
    }

    public function getAspectRatio(): AspectRatio
    {
        return $this->aspectRatio;
    }

    public function setAspectRatio(AspectRatio $aspectRatio): AspectRatio
    {
        $this->aspectRatio = $aspectRatio;

        return $this->aspectRatio;
    }

    public function hasPixelDensities(): bool
    {
        return $this->pixelDensities !== [];
    }

    public function getPixelDensities(): array
    {
        return $this->pixelDensities === [] ? [1] : $this->pixelDensities;
    }

    public function setPixelDensities(array $pixelDensities): array
    {
        $this->pixelDensities = array_values($pixelDensities);

        if (count($this->pixelDensities) !== count(array_filter($this->pixelDensities, 'is_numeric'))) {
            throw new InvalidArgumentException('PixelDensities can contain only numeric values.', 1536408342);
        }
        foreach ($this->pixelDensities as &$pixelDensity) {
            $pixelDensity = (float)$pixelDensity;
        }

        return $this->pixelDensities;
    }

    public function hasRespectFocusArea(): bool
    {
        return $this->respectFocusArea !== null;
    }

    public function getRespectFocusArea(): bool
    {
        return $this->respectFocusArea === null ? true : $this->respectFocusArea;
    }

    public function setRespectFocusArea(bool $respectFocusArea): bool
    {
        $this->respectFocusArea = $respectFocusArea;

        return $this->respectFocusArea;
    }

    public function hasTag(): bool
    {
        return !!$this->tag;
    }

    public function getTag(): TagBuilder
    {
        return $this->tag;
    }

    public function setTag(TagBuilder $tag): TagBuilder
    {
        $this->tag = $tag;

        return $this->tag;
    }
}
