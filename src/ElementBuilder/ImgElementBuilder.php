<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\ElementBuilder;

use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\Element\ImgElement;
use ByteCube\ImageTools\Image\ImageInterface;

class ImgElementBuilder extends AbstractImageElementBuilder
{
    /**
     * @var int
     */
    protected $fallbackWidth = 0;

    public function setFallbackWidth(int $fallbackWidth): int
    {
        $this->fallbackWidth = $fallbackWidth < 0 ? 0 : $fallbackWidth;

        return $this->fallbackWidth;
    }

    public function build(ImageInterface $image = null): ImgElement
    {
        if ($image === null) {
            $image = $this->image;
        }

        $fallback = null;
        if ($this->fallbackWidth > 0) {
            $cropInstructions = $this->instantiator->instantiate(CropInstructions::class);
            $cropInstructions->setCropName($this->getCropName());
            $cropInstructions->setAspectRatio($this->getAspectRatio());
            $cropInstructions->setRespectFocusArea($this->getRespectFocusArea());
            $cropInstructions->setWidth($this->fallbackWidth);
            $cropInstructions->setPixelDensity(1);

            $fallback = $image->crop($cropInstructions);
        }

        $processedImages = $this->process($image);
        $imgElement = $this->instantiator->instantiate(ImgElement::class, $processedImages, $fallback);

        if ($this->hasTag()) {
            $imgElement->setTag($this->getTag());
        }

        if ($this->placeholder) {
            $imgElement->setPlaceholder($this->placeholder);
        }

        return $imgElement;
    }
}
