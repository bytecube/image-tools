<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\ElementBuilder;

use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\Image\ImageInterface;

abstract class AbstractImageElementBuilder extends AbstractElementBuilder
{
    /**
     * @var int
     */
    protected $width = 0;

    /**
     * @var ImageInterface
     */
    protected $image;

    public function setWidth(int $width): int
    {
        $this->width = $width < 0 ? 0 : $width;

        return $this->width;
    }

    public function setImage(ImageInterface $image): ImageInterface
    {
        $this->image = $image;

        return $this->image;
    }

    protected function process(ImageInterface $image): array
    {
        $processedImages = [];

        foreach ($this->getPixelDensities() as $pixelDensity) {
            $cropInstructions = $this->instantiator->instantiate(CropInstructions::class);
            $cropInstructions->setCropName($this->getCropName());
            $cropInstructions->setAspectRatio($this->getAspectRatio());
            $cropInstructions->setRespectFocusArea($this->getRespectFocusArea());
            $cropInstructions->setWidth($this->width);
            $cropInstructions->setPixelDensity($pixelDensity);

            $processedImages[] = $image->crop($cropInstructions);
        }

        return $processedImages;
    }
}
