<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\ElementBuilder;

use ByteCube\ImageTools\Element\PictureElement;
use ByteCube\ImageTools\Element\SourceElementsGroup;
use ByteCube\ImageTools\Image\ImageInterface;

class PictureElementBuilder extends AbstractElementBuilder
{
    /**
     * @var SourceElementBuilder[]
     */
    protected $sourceElementBuilders = [];

    /**
     * @var ImgElementBuilder
     */
    protected $imgElementBuilder;

    public function hasSourceElementBuilder(): bool
    {
        return !empty($this->sourceElementBuilders);
    }

    public function hasImgElementBuilder(): bool
    {
        return $this->imgElementBuilder !== null;
    }

    public function createSourceElementBuilder(): SourceElementBuilder
    {
        $builder = $this->instantiator->instantiate(SourceElementBuilder::class);
        if ($this->placeholder) {
            $builder->setPlaceholder($this->placeholder);
        }
        $this->sourceElementBuilders[] = $builder;

        return $builder;
    }

    public function createImgElementBuilder(): ImgElementBuilder
    {
        $this->imgElementBuilder = $this->instantiator->instantiate(ImgElementBuilder::class);
        if ($this->placeholder) {
            $this->imgElementBuilder->setPlaceholder($this->placeholder);
        }

        return $this->imgElementBuilder;
    }

    public function build(ImageInterface $image): PictureElement
    {
        $sourceElements = $this->instantiator->instantiate(SourceElementsGroup::class);
        foreach ($this->sourceElementBuilders as $sourceElementBuilder) {
            $this->setCropNameToElement($sourceElementBuilder);
            $this->setAspectRatioToElement($sourceElementBuilder);
            $this->setPixelDensitiesToElement($sourceElementBuilder);
            $this->setRespectFocusAreaToElement($sourceElementBuilder);

            $sourceElements->add($sourceElementBuilder->build($image));
        }

        $this->setCropNameToElement($this->imgElementBuilder);
        $this->setAspectRatioToElement($this->imgElementBuilder);
        $this->setRespectFocusAreaToElement($this->imgElementBuilder);

        $imgElement = $this->imgElementBuilder->build($image);

        $pictureElement = $this->instantiator->instantiate(PictureElement::class, $sourceElements, $imgElement);

        if ($this->hasTag()) {
            $pictureElement->setTag($this->getTag());
        }

        return $pictureElement;
    }

    protected function setCropNameToElement(AbstractImageElementBuilder $elementBuilder)
    {
        if ($this->hasCropName() && !$elementBuilder->hasCropName()) {
            $elementBuilder->setCropName($this->getCropName());
        }
    }

    protected function setAspectRatioToElement(AbstractImageElementBuilder $elementBuilder)
    {
        if ($this->hasAspectRatio() && !$elementBuilder->hasAspectRatio()) {
            $elementBuilder->setAspectRatio($this->getAspectRatio());
        }
    }

    protected function setPixelDensitiesToElement(AbstractImageElementBuilder $elementBuilder)
    {
        if ($this->hasPixelDensities() && !$elementBuilder->hasPixelDensities()) {
            $elementBuilder->setPixelDensities($this->getPixelDensities());
        }
    }

    protected function setRespectFocusAreaToElement(AbstractImageElementBuilder $elementBuilder)
    {
        if ($this->hasRespectFocusArea() && !$elementBuilder->hasRespectFocusArea()) {
            $elementBuilder->setRespectFocusArea($this->getRespectFocusArea());
        }
    }
}
