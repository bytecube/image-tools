<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools;

use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageService\ImageServiceInterface;

class Processor
{
    /**
     * @var ImageInterface
     */
    protected $image;

    /**
     * @var ImageServiceInterface
     */
    protected $imageService;

    public function __construct(ImageInterface $image, ImageServiceInterface $imageService)
    {
        $this->image = $image;
        $this->imageService = $imageService;
    }

    public function crop(CropInstructions $cropInstructions): ImageInterface
    {
        $cropName = $cropInstructions->getCropName();

        $cropCollection = $this->image->getCrop();
        $crop = $cropCollection->get($cropName)
            ->withAspectRatio($cropInstructions->getAspectRatio());

        if (!$cropInstructions->isRespectFocusArea()) {
            $crop = $crop->withoutFocusArea();
        }

        $maxWidth = (int)($cropInstructions->getWidth() * $cropInstructions->getPixelDensity());

        $imageInfo = $this->image->getImageInfo();

        $width = $imageInfo->getWidth();
        $height = $imageInfo->getHeight();

        if ($maxWidth > $width) {
            $maxWidth = $width;
        }

        $maxWidth = $maxWidth > 0 ? $maxWidth : null;
        $cropDimensions = $crop->calculate($width, $height);

        return $this->imageService->crop($this->image, $maxWidth, $cropInstructions->getPixelDensity(), $cropDimensions);
    }

    public function blur(): ImageInterface
    {
        return $this->imageService->blur($this->image);
    }

    public function createColorImage(string $color): ImageInterface
    {
        return $this->imageService->createColorImage($this->image, $color);
    }
}
