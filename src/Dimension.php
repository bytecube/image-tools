<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools;

use Webmozart\Assert\Assert;

final class Dimension
{
    /**
     * @var Instantiator
     */
    private $instantiator;

    /**
     * @var float
     */
    private $x;

    /**
     * @var float
     */
    private $y;

    /**
     * @var float
     */
    private $width;

    /**
     * @var float
     */
    private $height;

    public function __construct(float $x, float $y, float $width, float $height, Instantiator $instantiator = null)
    {
        $this->instantiator = $instantiator ?: new Instantiator();
        $this->setX($x);
        $this->setY($y);
        $this->setWidth($width);
        $this->setHeight($height);
    }

    public function getX(): float
    {
        return $this->x;
    }

    public function setX(float $x): void
    {
        Assert::greaterThanEq($x, 0);
        Assert::lessThanEq($x, 1);

        $this->x = $x;
    }

    public function getY(): float
    {
        return $this->y;
    }

    public function setY(float $y): void
    {
        Assert::greaterThanEq($y, 0);
        Assert::lessThanEq($y, 1);

        $this->y = $y;
    }

    public function getWidth(): float
    {
        return $this->width;
    }

    public function setWidth(float $width): void
    {
        Assert::greaterThanEq($width, 0);
        Assert::lessThanEq($width, 1);

        $this->width = $width;
    }

    public function getHeight(): float
    {
        return $this->height;
    }

    public function setHeight(float $height): void
    {
        Assert::greaterThanEq($height, 0);
        Assert::lessThanEq($height, 1);

        $this->height = $height;
    }

    public function rotate(): Dimension
    {
        return $this->instantiator->instantiate(Dimension::class, $this->y, $this->x, $this->height, $this->width);
    }

    public function getAspectRatio(): AspectRatio
    {
        return $this->instantiator->instantiate(AspectRatio::class, $this->width, $this->height);
    }
}
