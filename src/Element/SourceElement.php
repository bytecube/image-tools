<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Element;

use ByteCube\ImageTools\Instantiator;
use InvalidArgumentException;

class SourceElement extends AbstractImageElement
{
    public function __construct(array $images, Instantiator $instantiator = null)
    {
        parent::__construct($images, $instantiator);

        if (count($images) < 1) {
            throw new InvalidArgumentException('Source element needs at least one processed image.', 1536408398);
        }
    }

    protected function render(array $imageUris, ?string $placeholder): string
    {
        $srcsetAttributeName = 'srcset';

        $this->resetTag();

        if ($placeholder) {
            $srcsetAttributeName = 'data-srcset';

            $this->tag->addAttribute('srcset', $placeholder);
        }

        $srcset = [];
        foreach ($imageUris as $pixelDensity => $imageUri) {
            $srcset[] = count($srcset) === 0
                ? $imageUri
                : $imageUri . ' ' . $pixelDensity . 'x';
        }
        $this->tag->addAttribute($srcsetAttributeName, implode(', ', $srcset));

        return $this->tag->render();
    }

    protected function resetTag()
    {
        parent::resetTag();

        $this->tag->setTagName('source');
    }
}
