<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Element;

use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\URLResolver;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

abstract class AbstractElement
{
    /**
     * @var Instantiator
     */
    protected $instantiator;

    /**
     * @var URLResolver
     */
    protected $urlResolver;

    /**
     * @var TagBuilder
     */
    protected $tag;

    public function __construct(Instantiator $instantiator = null)
    {
        $this->instantiator = $instantiator ?: new Instantiator();
        $this->urlResolver = $this->instantiator->instantiate(URLResolver::class);
        $this->tag = new TagBuilder();
    }

    abstract public function getImageList(): array;

    public function setTag(TagBuilder $tag): void
    {
        $this->tag = $tag;
    }

    public function getTag(): TagBuilder
    {
        return $this->tag;
    }

    public function getRelativeImageURLs(): array
    {
        return $this->urlResolver->resolveRelative($this->getImageList());
    }

    public function getAbsoluteImageURLs(): array
    {
        return $this->urlResolver->resolveAbsolute($this->getImageList());
    }
}
