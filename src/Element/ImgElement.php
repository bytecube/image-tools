<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Element;

use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\Instantiator;
use InvalidArgumentException;

class ImgElement extends AbstractImageElement
{
    /**
     * @var ImageInterface
     */
    protected $fallbackImage;

    public function __construct(array $images, ImageInterface $fallbackImage = null, Instantiator $instantiator = null)
    {
        parent::__construct($images, $instantiator);

        if (count($images) > 1 && $fallbackImage === null) {
            throw new InvalidArgumentException('If more than one processed images given, a fallback image is required.', 1536408374);
        }

        $this->fallbackImage = $fallbackImage;
    }

    public function getFallback(): ImageInterface
    {
        return $this->fallbackImage;
    }

    public function getImageList(): array
    {
        if (count($this->images) === 1) {
            return [
                'src' => $this->images[0],
            ];
        }

        return [
            'src' => $this->fallbackImage,
            'srcset' => parent::getImageList(),
        ];
    }

    protected function render(array $imageUris, ?string $placeholder): string
    {
        $srcAttributeName = 'src';
        $srcsetAttributeName = 'srcset';

        $this->resetTag();

        if ($placeholder) {
            $srcAttributeName = 'data-src';
            $srcsetAttributeName = 'data-srcset';

            $this->tag->addAttribute('src', $placeholder);
        }

        $this->tag->addAttribute($srcAttributeName, $imageUris['src']);

        if (isset($imageUris['srcset'])) {
            $srcsetParts = [];
            foreach ($imageUris['srcset'] as $pixelDensity => $imageUri) {
                $srcsetParts[] = count($srcsetParts) === 0
                    ? $imageUri
                    : $imageUri . ' ' . $pixelDensity . 'x';
            }
            $this->tag->addAttribute($srcsetAttributeName, implode(', ', $srcsetParts));
        }

        return $this->tag->render();
    }

    protected function resetTag()
    {
        parent::resetTag();

        $this->tag->setTagName('img');
        $this->tag->removeAttribute('src');
        $this->tag->removeAttribute('data-src');
    }
}
