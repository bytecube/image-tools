<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Element;

use ByteCube\ImageTools\Instantiator;
use InvalidArgumentException;

class PictureElement extends AbstractElement
{
    /**
     * @var SourceElementsGroup
     */
    protected $sourceElements;

    /**
     * @var ImgElement
     */
    protected $imgElement;

    public function __construct(
        SourceElementsGroup $sourceElementsGroup,
        ImgElement $imgElement,
        Instantiator $instantiator = null
    ) {
        parent::__construct($instantiator);

        if ($sourceElementsGroup->isEmpty()) {
            throw new InvalidArgumentException('Picture element requires at least one source element.', 1536408386);
        }

        $this->sourceElements = $sourceElementsGroup;
        $this->imgElement = $imgElement;
    }

    public function getSourceElements(): SourceElementsGroup
    {
        return $this->sourceElements;
    }

    public function getImgElement(): ImgElement
    {
        return $this->imgElement;
    }

    public function getImageList(): array
    {
        return [
            'sources' => $this->sourceElements->getImageList(),
            'img' => $this->imgElement->getImageList(),
        ];
    }

    public function renderRelative(): string
    {
        return $this->render(
            $this->sourceElements->renderRelative(),
            $this->imgElement->renderRelative()
        );
    }

    public function renderAbsolute(): string
    {
        return $this->render(
            $this->sourceElements->renderAbsolute(),
            $this->imgElement->renderAbsolute()
        );
    }

    protected function render(string $sourceTags, string $imgTag): string
    {
        $this->tag->setTagName('picture');
        $this->tag->setContent($sourceTags . $imgTag);

        return $this->tag->render();
    }
}
