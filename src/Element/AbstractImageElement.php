<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Element;

use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\Instantiator;
use ByteCube\ImageTools\Placeholder;

abstract class AbstractImageElement extends AbstractElement
{
    /**
     * @var ImageInterface[]
     */
    protected $images = [];

    /**
     * @var Placeholder
     */
    protected $placeholder;

    public function __construct(array $images, Instantiator $instantiator = null)
    {
        parent::__construct($instantiator);

        foreach ($images as $image) {
            $this->addImage($image);
        }
    }

    /**
     * @return ImageInterface[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    public function setPlaceholder(Placeholder $placeholder): void
    {
        $this->placeholder = $placeholder;
    }

    public function renderRelative(): string
    {
        return $this->render(
            $this->getRelativeImageURLs(),
            $this->createRelativePlaceholderUri()
        );
    }

    public function renderAbsolute(): string
    {
        return $this->render(
            $this->getAbsoluteImageURLs(),
            $this->createAbsolutePlaceholderUri()
        );
    }

    public function getImageList(): array
    {
        $imageUris = [];
        foreach ($this->images as $image) {
            $imageUris[(string)$image->getPixelDensity()] = $image;
        }

        return $imageUris;
    }

    abstract protected function render(array $imageUris, ?string $placeholder): string;

    protected function addImage(ImageInterface $image)
    {
        $this->images[] = $image;
    }

    protected function resetTag()
    {
        $this->tag->removeAttribute('srcset');
        $this->tag->removeAttribute('data-srcset');
    }

    protected function createRelativePlaceholderUri(): ?string
    {
        return $this->placeholder
            ? $this->processPlaceholder()->getImageInfo()->getRelativeURL()
            : null;
    }

    protected function createAbsolutePlaceholderUri(): ?string
    {
        return $this->placeholder
            ? $this->processPlaceholder()->getImageInfo()->getAbsoluteURL()
            : null;
    }

    protected function processPlaceholder(): ImageInterface
    {
        return $this->placeholder->process($this->images[0]);
    }
}
