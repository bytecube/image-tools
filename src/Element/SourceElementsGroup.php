<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Element;

class SourceElementsGroup
{
    /**
     * @var SourceElement[]
     */
    protected $elements = [];

    /**
     * @return SourceElement[]
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    public function add(SourceElement $element): void
    {
        $this->elements[] = $element;
    }

    public function isEmpty(): bool
    {
        return empty($this->elements);
    }

    public function renderRelative(): string
    {
        $result = '';
        foreach ($this->elements as $element) {
            $result .= $element->renderRelative();
        }

        return $result;
    }

    public function renderAbsolute(): string
    {
        $result = '';
        foreach ($this->elements as $element) {
            $result .= $element->renderAbsolute();
        }

        return $result;
    }

    public function getImageList(): array
    {
        $imageUris = [];
        foreach ($this->elements as $element) {
            $imageUris[] = $element->getImageList();
        }

        return $imageUris;
    }
}
