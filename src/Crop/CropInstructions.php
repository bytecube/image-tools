<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Crop;

use ByteCube\ImageTools\AspectRatio;

final class CropInstructions
{
    /**
     * @var string
     */
    protected $cropName = '';

    /**
     * @var float
     */
    protected $width = 0.0;

    /**
     * @var float
     */
    protected $pixelDensity = 1.0;

    /**
     * @var AspectRatio
     */
    protected $aspectRatio;

    /**
     * @var bool
     */
    protected $respectFocusArea = true;

    public function getCropName(): string
    {
        return $this->cropName;
    }

    public function setCropName(string $cropName)
    {
        $this->cropName = $cropName;
    }

    public function getWidth(): float
    {
        return $this->width;
    }

    public function setWidth(float $width)
    {
        $this->width = $width;
    }

    public function getPixelDensity(): float
    {
        return $this->pixelDensity;
    }

    public function setPixelDensity(float $pixelDensity)
    {
        $this->pixelDensity = $pixelDensity;
    }

    public function getAspectRatio(): AspectRatio
    {
        return $this->aspectRatio;
    }

    public function setAspectRatio(AspectRatio $aspectRatio): void
    {
        $this->aspectRatio = $aspectRatio;
    }

    public function isRespectFocusArea(): bool
    {
        return $this->respectFocusArea;
    }

    public function setRespectFocusArea(bool $respectFocusArea)
    {
        $this->respectFocusArea = $respectFocusArea;
    }
}
