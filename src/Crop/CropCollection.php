<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Crop;

use ByteCube\ImageTools\Instantiator;

class CropCollection
{
    /**
     * @var Instantiator
     */
    private $instantiator;

    /**
     * @var Crop[]
     */
    private $cropCollection = [];

    public function __construct(Instantiator $instantiator = null)
    {
        $this->instantiator = $instantiator ?: new Instantiator();
    }

    public function get(string $name): Crop
    {
        return isset($this->cropCollection[$name]) ? $this->cropCollection[$name] : $this->instantiator->instantiate(Crop::class);
    }

    public function add(string $name, Crop $crop)
    {
        $this->cropCollection[$name] = $crop;
    }
}
