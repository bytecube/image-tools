<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Crop;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Dimension;
use ByteCube\ImageTools\Instantiator;

class Crop
{
    /**
     * @var Instantiator
     */
    private $instantiator;

    /**
     * @var Dimension|null
     */
    private $cropArea;

    /**
     * @var AspectRatio|null
     */
    private $aspectRatio;

    /**
     * @var Dimension|null
     */
    private $focusArea;

    public function __construct(Instantiator $instantiator = null)
    {
        $this->instantiator = $instantiator ?: new Instantiator();
    }

    public function withCropArea(Dimension $cropArea): Crop
    {
        $crop = clone $this;
        $crop->cropArea = $cropArea;

        return $crop;
    }

    public function withAspectRatio(AspectRatio $aspectRatio): Crop
    {
        $crop = clone $this;
        $crop->aspectRatio = $aspectRatio;

        return $crop;
    }

    public function withFocusArea(Dimension $focusArea): Crop
    {
        $crop = clone $this;
        $crop->focusArea = $focusArea;

        return $crop;
    }

    public function withoutFocusArea(): Crop
    {
        $crop = clone $this;
        $crop->focusArea = null;

        return $crop;
    }

    public function calculate(int $width, int $height): CropDimensions
    {
        $cropArea = $this->cropArea;
        if (!$cropArea) {
            $cropArea = $this->instantiator->instantiate(Dimension::class, 0, 0, 1, 1);
        }

        if (!$this->aspectRatio || !$this->aspectRatio->isValid()) {
            return $this->calculateResult(
                $cropArea,
                $width,
                $height
            );
        }

        $sourceAspectRatio = $this->instantiator->instantiate(AspectRatio::class, $width, $height);

        $rotate = $this->aspectRatio->get() > $sourceAspectRatio->get();

        if ($rotate) {
            $result = $this->calculateCrop(
                $cropArea->rotate(),
                $sourceAspectRatio->rotate(),
                $this->aspectRatio->rotate(),
                $this->focusArea ? $this->focusArea->rotate() : null
            )->rotate();

            return $this->calculateResult($result, $width, $height);
        }

        $result = $this->calculateCrop(
            $cropArea,
            $sourceAspectRatio,
            $this->aspectRatio,
            $this->focusArea
        );

        return $this->calculateResult($result, $width, $height);
    }

    private function calculateResult(Dimension $result, float $width, float $height): CropDimensions
    {
        return $this->instantiator->instantiate(
            CropDimensions::class,
            (int)round($result->getX() * $width),
            (int)round($result->getY() * $height),
            (int)round($result->getWidth() * $width),
            (int)round($result->getHeight() * $height)
        );
    }

    private function calculateCrop(
        Dimension $cropArea,
        AspectRatio $sourceAspectRatio,
        AspectRatio $targetAspectRatio,
        ?Dimension $focusArea
    ): Dimension {
        $newWidth = ($cropArea->getHeight() * $targetAspectRatio->get()) / $sourceAspectRatio->get();

        $newX = $focusArea
            ? $this->applyFocus($cropArea, $focusArea, $newWidth)
            : (($cropArea->getWidth() - $newWidth) / 2) + $cropArea->getX();

        return $this->instantiator->instantiate(Dimension::class, $newX, $cropArea->getY(), $newWidth, $cropArea->getHeight());
    }

    private function applyFocus(Dimension $cropArea, Dimension $focusArea, float $newWidth): float
    {
        [$focusWidth, $focusLeft] = $this->calculateFocus($cropArea, $focusArea);
        $focusRight = $focusWidth + $focusLeft;

        if ($focusWidth > $newWidth) {
            $diff = $focusWidth - $newWidth;

            return $focusLeft + ($diff / 2);
        }

        $focusLeftMarginToCrop = $focusLeft - $cropArea->getX();
        $focusRightMarginToCrop = ($cropArea->getX() + $cropArea->getWidth()) - $focusRight;

        $leftSpaceRelative = $focusLeftMarginToCrop / ($focusLeftMarginToCrop + $focusRightMarginToCrop);
        $widthDiff = $cropArea->getWidth() - $newWidth;

        return $cropArea->getX() + ($leftSpaceRelative * $widthDiff);
    }

    private function calculateFocus(Dimension $cropArea, Dimension $focusArea): array
    {
        $size = ($focusArea->getWidth() ?: 1.0) * $cropArea->getWidth();
        $position = ($focusArea->getX() * $cropArea->getWidth()) + $cropArea->getX();

        return [$size, $position];
    }
}
