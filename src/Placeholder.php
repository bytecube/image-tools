<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools;

use ByteCube\ImageTools\Image\ImageInterface;
use InvalidArgumentException;

class Placeholder
{
    /**
     * @var string
     */
    private $placeholder;

    public function __construct(string $placeholder)
    {
        $this->placeholder = $placeholder;
    }

    public function process(ImageInterface $image): ImageInterface
    {
        if ($this->placeholder[0] === '#') {
            $color = substr($this->placeholder, 1);

            return $image->createColorImage($color);
        }

        if ($this->placeholder === 'blurred') {
            return $image->blur();
        }

        throw new InvalidArgumentException('Invalid placeholder definition "' . $this->placeholder . '".', 1558022567);
    }
}
