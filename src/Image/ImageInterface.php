<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Image;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Crop\CropCollection;
use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\StorageInterface;

interface ImageInterface
{
    public function getImageInfo(): ImageInfoInterface;

    public function getAspectRatio(): AspectRatio;

    public function getPixelDensity(): float;

    public function getCrop(): CropCollection;

    public function getOriginal();

    public function getStorage(): StorageInterface;

    public function crop(CropInstructions $cropInstructions): ImageInterface;

    public function blur(): ImageInterface;

    public function createColorImage(string $color): ImageInterface;
}
