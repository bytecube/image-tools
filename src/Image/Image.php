<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools\Image;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\Crop\CropCollection;
use ByteCube\ImageTools\Crop\CropInstructions;
use ByteCube\ImageTools\ImageInfoInterface;
use ByteCube\ImageTools\ImageService\ImageServiceInterface;
use ByteCube\ImageTools\Processor;
use ByteCube\ImageTools\StorageInterface;

class Image implements ImageInterface
{
    /**
     * @var ImageInfoInterface
     */
    private $imageInfo;

    /**
     * @var AspectRatio
     */
    private $aspectRatio;

    /**
     * @var float
     */
    private $pixelDensity;

    /**
     * @var CropCollection
     */
    private $crop;

    /**
     * @var mixed
     */
    private $original;

    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @var Processor
     */
    private $processor;

    public function __construct(
        ImageInfoInterface $imageInfo,
        $original,
        StorageInterface $storage,
        ImageServiceInterface $imageService,
        ?float $pixelDensity = null,
        ?CropCollection $crop = null
    ) {
        $this->imageInfo = $imageInfo;
        $this->aspectRatio = new AspectRatio($imageInfo->getWidth(), $imageInfo->getHeight());
        $this->pixelDensity = $pixelDensity ?: 1;
        $this->crop = $crop ?: new CropCollection();
        $this->original = $original;
        $this->storage = $storage;
        $this->processor = new Processor($this, $imageService);
    }

    public function getImageInfo(): ImageInfoInterface
    {
        return $this->imageInfo;
    }

    public function getAspectRatio(): AspectRatio
    {
        return $this->aspectRatio;
    }

    public function getPixelDensity(): float
    {
        return $this->pixelDensity;
    }

    public function getCrop(): CropCollection
    {
        return $this->crop;
    }

    public function getOriginal()
    {
        return $this->original;
    }

    public function getStorage(): StorageInterface
    {
        return $this->storage;
    }

    public function crop(CropInstructions $cropInstructions): ImageInterface
    {
        return $this->processor->crop($cropInstructions);
    }

    public function blur(): ImageInterface
    {
        return $this->processor->blur();
    }

    public function createColorImage(string $color): ImageInterface
    {
        return $this->processor->createColorImage($color);
    }
}
