<?php declare(strict_types = 1);
/**
 * This file is part of ByteCube/ImageTools.
 *
 * ByteCube/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ByteCube/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ByteCube/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ByteCube\ImageTools;

use ByteCube\ImageTools\Image\ImageInterface;
use InvalidArgumentException;

class URLResolver
{
    public function resolveRelative(array $list): array
    {
        return $this->resolve($list, false);
    }

    public function resolveAbsolute(array $list): array
    {
        return $this->resolve($list, true);
    }

    private function resolve(array $list, bool $absolutePath): array
    {
        foreach ($list as &$item) {
            if (is_array($item)) {
                $item = $this->resolve($item, $absolutePath);
                continue;
            }

            if ($item instanceof ImageInterface) {
                $item = $absolutePath
                    ? $item->getImageInfo()->getAbsoluteURL()
                    : $item->getImageInfo()->getRelativeURL();

                continue;
            }

            throw new InvalidArgumentException('Invalid item "' . get_class($item) . '" to generate uri.', 1558105700);
        }

        return $list;
    }
}
